# Regression tracking bot designed for the Linux kernel

Regzbot is a bot tailored for low-overhead regression tracking in the email
driven Linux kernel development process. It's currently WIP; the regzbot
developer started to use it now in a kind of alpha testing.

Regzbot is still in the beta phase, but you are free to use it. See
[getting started with regzbot](docs/getting_started.md) or the bots
[reference documentation](docs/reference.md) for details how to use
regzbot. To get an impression how the data is processed, checkout out
[regzbot's webinterface](https://linux-regtracking.leemhuis.info/regzbot/mainline/).

For some more background about the whole idea for a bot see here:
https://linux-regtracking.leemhuis.info/post/regzbot-approach/

## Licensing

Rezbot is available under the APGL 3.0; see the file COPYING for details. If
you think a more liberal license should be used, let Thorsten know what you'd
prefer, as for now it's still quite easy to change the license.

Regzbot was started by Thorsten Leemhuis as part of a project that has received
funding from the European Union’s Horizon 2020 research and innovation
programme under grant agreement No 871528.
